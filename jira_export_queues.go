package norad

import (
	"fmt"
)

type JiraExportQueuesService struct {
	client NoradClient
}

type JiraExportQueue struct {
	ResultExportQueue
	QueueConfig JiraExportQueueConfiguration `json:"jira_export_queue_configuration"`
}

type JiraExportQueueUpdateOptions struct {
	Parameters JiraExportQueueUpdateParameters `json:"jira_export_queue"`
}

type JiraExportQueueCreateOptions struct {
	Parameters JiraExportQueueCreateParameters `json:"jira_export_queue"`
}

type JiraExportQueueUpdateParameters struct {
	AutoSync    bool                         `json:"auto_sync"`
	QueueConfig JiraExportQueueConfiguration `json:"custom_jira_configuration_attributes"`
}

type JiraExportQueueCreateParameters struct {
	AutoSync    bool                               `json:"auto_sync"`
	QueueConfig JiraExportQueueConfigCreateOptions `json:"custom_jira_configuration_attributes"`
}

type JiraExportQueueConfigCreateOptions struct {
	JiraExportQueueConfigurationAttributes
}

type JiraExportQueueConfiguration struct {
	Id int `json:"id"`
	JiraExportQueueConfigurationAttributes
}

type JiraExportQueueConfigurationAttributes struct {
	Title      string `json:"title"`
	Username   string `json:"username"`
	Password   string `json:"password"`
	SiteUrl    string `json:"site_url"`
	ProjectKey string `json:"project_key"`
}

func (obj JiraExportQueue) String() string {
	return Stringify(obj)
}

func (service *JiraExportQueuesService) Create(oid interface{}, opt *JiraExportQueueCreateOptions) (*JiraExportQueue, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/jira_export_queues", org_id)
	req, err := service.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	jira_export_queue := new(JiraExportQueue)
	resp, err := service.client.Do(req, jira_export_queue)
	if err != nil {
		return nil, resp, err
	}

	return jira_export_queue, resp, err
}

func (service *JiraExportQueuesService) Delete(id interface{}) (*Response, error) {
	jira_export_queue_id, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("jira_export_queues/%s", jira_export_queue_id)
	req, err := service.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return service.client.Do(req, nil)
}

func (service *JiraExportQueuesService) Get(id interface{}) (*JiraExportQueue, *Response, error) {
	channel_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	// We have to look up the associated org and pluck its config
	u := fmt.Sprintf("jira_export_queues/%s", channel_id)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	jira_export_queues := new(JiraExportQueue)
	resp, err := service.client.Do(req, jira_export_queues)
	if err != nil {
		return nil, resp, err
	}

	return jira_export_queues, resp, err
}

func (service *JiraExportQueuesService) Update(id interface{}, opt *JiraExportQueueUpdateOptions) (*JiraExportQueue, *Response, error) {
	channel_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("jira_export_queues/%s", channel_id)
	req, err := service.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	jira_export_queues := new(JiraExportQueue)
	resp, err := service.client.Do(req, jira_export_queues)
	if err != nil {
		return nil, resp, err
	}

	return jira_export_queues, resp, err
}
