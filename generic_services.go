package norad

import (
	"fmt"
)

type GenericServicesService struct {
	client *Client
}

type GenericService struct {
	Id              int    `json:"id"`
	Name            string `json:"name"`
	Description     string `json:"description"`
	Port            int    `json:"port"`
	PortType        string `json:"port_type"`
	EncryptionType  string `json:"encryption_type"`
	Type            string `json:"type"`
	AllowBruteForce bool   `json:"allow_brute_force"`
	Discovered      bool   `json:"discovered"`
	MachineId       int    `json:"machine_id"`
}

type CreateGenericServiceOptions struct {
	GenericServiceCreationParameters `json:"service"`
}

type UpdateGenericServiceOptions struct {
	GenericServiceUpdateParameters `json:"service"`
}

type GenericServiceCreationParameters struct {
	Name            string `json:"name"`
	Description     string `json:"description"`
	Port            string `json:"port"`
	PortType        string `json:"port_type"`
	EncryptionType  string `json:"encryption_type"`
	Type            string `json:"type"`
	AllowBruteForce bool   `json:"allow_brute_force"`
}

type GenericServiceUpdateParameters struct {
	Name            string `json:"name"`
	Description     string `json:"description"`
	Port            string `json:"port"`
	PortType        string `json:"port_type"`
	EncryptionType  string `json:"encryption_type"`
	AllowBruteForce bool   `json:"allow_brute_force"`
}

func (m GenericService) String() string {
	return Stringify(m)
}

func (s *GenericServicesService) GetGenericService(id interface{}) (*GenericService, *Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("services/%s", sid)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *GenericService
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *GenericServicesService) CreateGenericService(oid interface{}, opt *CreateGenericServiceOptions) (*GenericService, *Response, error) {
	machine_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}
	opt.GenericServiceCreationParameters.Type = "GenericService"

	u := fmt.Sprintf("machines/%s/services", machine_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(GenericService)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *GenericServicesService) UpdateGenericService(id interface{}, opt *UpdateGenericServiceOptions) (*GenericService, *Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("services/%s", sid)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(GenericService)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *GenericServicesService) DeleteGenericService(id interface{}) (*Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("services/%s", sid)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
