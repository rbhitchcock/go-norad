package norad

import (
	"fmt"
)

type NotificationChannelsService struct {
	client NoradClient
}

type NotificationChannel struct {
	Id             int `json:"id"`
	OrganizationId int `json:"organization_id"`
	Enabled        bool
	Event          string
	Description    string
}

type NotificationChannelOptions struct {
	Parameters NotificationChannelParameters `json:"notification_channel"`
}

type NotificationChannelParameters struct {
	Enabled bool `json:"enabled"`
}

func (obj NotificationChannel) String() string {
	return Stringify(obj)
}

func (service *NotificationChannelsService) List(oid interface{}) ([]*NotificationChannel, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/notification_channels", org_id)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	notification_channels := make([]*NotificationChannel, 0)
	resp, err := service.client.Do(req, &notification_channels)
	if err != nil {
		return nil, resp, err
	}

	return notification_channels, resp, err
}

func (service *NotificationChannelsService) Get(id interface{}) (*NotificationChannel, *Response, error) {
	channel_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	// We have to look up the associated org and pluck its config
	u := fmt.Sprintf("notification_channels/%s", channel_id)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	notification_channel := new(NotificationChannel)
	resp, err := service.client.Do(req, notification_channel)
	if err != nil {
		return nil, resp, err
	}

	return notification_channel, resp, err
}

func (service *NotificationChannelsService) Update(id interface{}, opt *NotificationChannelOptions) (*NotificationChannel, *Response, error) {
	channel_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("notification_channels/%s", channel_id)
	req, err := service.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	notification_channel := new(NotificationChannel)
	resp, err := service.client.Do(req, notification_channel)
	if err != nil {
		return nil, resp, err
	}

	return notification_channel, resp, err
}
