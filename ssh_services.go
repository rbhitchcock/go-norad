package norad

import (
	"fmt"
)

type SshServicesService struct {
	client *Client
}

type SshService struct {
	Id              int    `json:"id"`
	Name            string `json:"name"`
	Description     string `json:"description"`
	Port            int    `json:"port"`
	PortType        string `json:"port_type"`
	EncryptionType  string `json:"encryption_type"`
	Type            string `json:"type"`
	AllowBruteForce bool   `json:"allow_brute_force"`
	Discovered      bool   `json:"discovered"`
	MachineId       int    `json:"machine_id"`
}

type CreateSshServiceOptions struct {
	SshServiceCreationParameters `json:"service"`
}

type UpdateSshServiceOptions struct {
	SshServiceUpdateParameters `json:"service"`
}

type SshServiceCreationParameters struct {
	Name            string `json:"name"`
	Description     string `json:"description"`
	Port            string `json:"port"`
	PortType        string `json:"port_type"`
	EncryptionType  string `json:"encryption_type"`
	Type            string `json:"type"`
	AllowBruteForce bool   `json:"allow_brute_force"`
}

type SshServiceUpdateParameters struct {
	Name            string `json:"name"`
	Description     string `json:"description"`
	Port            string `json:"port"`
	PortType        string `json:"port_type"`
	EncryptionType  string `json:"encryption_type"`
	AllowBruteForce bool   `json:"allow_brute_force"`
}

func (m SshService) String() string {
	return Stringify(m)
}

func (s *SshServicesService) GetSshService(id interface{}) (*SshService, *Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("services/%s", sid)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *SshService
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SshServicesService) CreateSshService(oid interface{}, opt *CreateSshServiceOptions) (*SshService, *Response, error) {
	machine_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}
	opt.SshServiceCreationParameters.Type = "SshService"
	opt.SshServiceCreationParameters.PortType = "tcp"
	opt.SshServiceCreationParameters.EncryptionType = "ssh"

	u := fmt.Sprintf("machines/%s/services", machine_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SshService)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SshServicesService) UpdateSshService(id interface{}, opt *UpdateSshServiceOptions) (*SshService, *Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}
	opt.SshServiceUpdateParameters.PortType = "tcp"
	opt.SshServiceUpdateParameters.EncryptionType = "ssh"

	u := fmt.Sprintf("services/%s", sid)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(SshService)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *SshServicesService) DeleteSshService(id interface{}) (*Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("services/%s", sid)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
