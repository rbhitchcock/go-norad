package norad

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestRequirementGroupServiceParseError(t *testing.T) {
	service := &RequirementGroupsService{}
	_, _, err := service.Get(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &RequirementGroupsService{}
	_, _, err = service.Update(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}
}

func TestRequirementGroupsServiceList(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing listing RequirementGroups with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"requirement_groups",
				nil,
			).Return(&http.Request{}, c.requestError)
			requirement_groups := make([]*RequirementGroup, 0)
			client.On(
				"Do",
				&http.Request{},
				&requirement_groups,
			).Return(&Response{}, c.doError)

			service := &RequirementGroupsService{client: &client}
			_, _, err := service.List()
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestRequirementGroupsServiceCreate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a RequirementGroup with expected error %s", c.expectedError), func(t *testing.T) {
			opts := RequirementGroupOptions{Parameters: RequirementGroupParameters{Name: "created"}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"POST",
				"requirement_groups",
				&opts,
			).Return(&http.Request{}, c.requestError)
			requirement_groups := new(RequirementGroup)
			client.On(
				"Do",
				&http.Request{},
				requirement_groups,
			).Return(&Response{}, c.doError)

			service := &RequirementGroupsService{client: &client}
			_, _, err := service.Create(&opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestRequirementGroupsServiceUpdate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a RequirementGroup with expected error %s", c.expectedError), func(t *testing.T) {
			opts := RequirementGroupOptions{Parameters: RequirementGroupParameters{Name: "updated"}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"PUT",
				"requirement_groups/1",
				&opts,
			).Return(&http.Request{}, c.requestError)
			requirement_group := new(RequirementGroup)
			client.On(
				"Do",
				&http.Request{},
				requirement_group,
			).Return(&Response{}, c.doError)

			service := &RequirementGroupsService{client: &client}
			_, _, err := service.Update(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}
