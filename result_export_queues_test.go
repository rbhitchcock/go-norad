package norad

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestResultExportQueueServiceParseError(t *testing.T) {
	service := &ResultExportQueuesService{}
	_, _, err := service.List(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}
}

func TestResultExportQueuesServiceList(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing listing ResultExportQueues with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"organizations/1/result_export_queues",
				nil,
			).Return(&http.Request{}, c.requestError)
			result_export_queues := make([]*ResultExportQueue, 0)
			client.On(
				"Do",
				&http.Request{},
				&result_export_queues,
			).Return(&Response{}, c.doError)

			service := &ResultExportQueuesService{client: &client}
			_, _, err := service.List(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}
