package norad

type User struct {
	Id        int
	Email     string
	Uid       string
	Firstname string
	Lastname  string
	ApiToken  string `json:"api_token"`
}
