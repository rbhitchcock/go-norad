package norad

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/google/go-querystring/query"
)

type NoradClient interface {
	NewRequest(string, string, interface{}) (*http.Request, error)
	Do(*http.Request, interface{}) (*Response, error)
}

type Client struct {
	client  *http.Client
	baseURL *url.URL
	token   string

	Enforcements               *EnforcementsService
	GenericServices            *GenericServicesService
	InfosecExportQueues        *InfosecExportQueuesService
	JiraExportQueues           *JiraExportQueuesService
	Machines                   *MachinesService
	Memberships                *MembershipsService
	NotificationChannels       *NotificationChannelsService
	Organizations              *OrganizationsService
	OrganizationConfigurations *OrganizationConfigurationsService
	Relays                     *RelaysService
	RequirementGroups          *RequirementGroupsService
	ResultExportQueues         *ResultExportQueuesService
	SecurityTests              *SecurityTestsService
	SecurityTestConfigs        *SecurityTestConfigsService
	SecurityTestRepositories   *SecurityTestRepositoriesService
	ServiceIdentities          *ServiceIdentitiesService
	SshKeyPairs                *SshKeyPairsService
	SshKeyPairAssignments      *SshKeyPairAssignmentsService
	SshServices                *SshServicesService
	WebApplicationConfigs      *WebApplicationConfigsService
	WebApplicationServices     *WebApplicationServicesService
}

func NewClient(httpClient *http.Client, norad_url string, api_token string, skip_tls_verify bool) *Client {
	if httpClient == nil {
		ssl_config := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: skip_tls_verify},
		}
		httpClient = &http.Client{Transport: ssl_config}
	}

	baseURL, _ := url.Parse(norad_url)

	c := &Client{client: httpClient, baseURL: baseURL, token: api_token}
	c.Organizations = &OrganizationsService{client: c}
	c.OrganizationConfigurations = &OrganizationConfigurationsService{client: c}
	c.Machines = &MachinesService{client: c}
	c.Memberships = &MembershipsService{client: c}
	c.NotificationChannels = &NotificationChannelsService{client: c}

	c.RequirementGroups = &RequirementGroupsService{client: c}
	c.Enforcements = &EnforcementsService{client: c}

	c.ResultExportQueues = &ResultExportQueuesService{client: c}
	c.InfosecExportQueues = &InfosecExportQueuesService{client: c}
	c.JiraExportQueues = &JiraExportQueuesService{client: c}

	c.SshServices = &SshServicesService{client: c}
	c.GenericServices = &GenericServicesService{client: c}
	c.WebApplicationServices = &WebApplicationServicesService{client: c}
	c.ServiceIdentities = &ServiceIdentitiesService{client: c}
	c.WebApplicationConfigs = &WebApplicationConfigsService{client: c}

	c.SecurityTestRepositories = &SecurityTestRepositoriesService{client: c}
	c.SecurityTests = &SecurityTestsService{client: c}
	c.SecurityTestConfigs = &SecurityTestConfigsService{client: c}

	c.SshKeyPairs = &SshKeyPairsService{client: c}
	c.SshKeyPairAssignments = &SshKeyPairAssignmentsService{client: c}

	c.Relays = &RelaysService{client: c}

	return c
}

func (c *Client) NewRequest(method string, path string, opt interface{}) (*http.Request, error) {
	u := *c.baseURL

	u.Opaque = c.baseURL.Path + path

	if opt != nil {
		q, err := query.Values(opt)
		if err != nil {
			return nil, err
		}
		u.RawQuery = q.Encode()
	}

	req := &http.Request{
		Method:     method,
		URL:        &u,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 1,
		Header:     make(http.Header),
		Host:       u.Host,
	}

	if method == "POST" || method == "PUT" {
		bodyBytes, err := json.Marshal(opt)
		if err != nil {
			return nil, err
		}
		bodyReader := bytes.NewReader(bodyBytes)

		u.RawQuery = ""
		req.Body = ioutil.NopCloser(bodyReader)
		req.ContentLength = int64(bodyReader.Len())
		req.Header.Set("Content-Type", "application/json")
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Token token=%s", c.token))

	fmt.Println("Request:", req)
	return req, nil
}

func (c *Client) Do(req *http.Request, v interface{}) (*Response, error) {
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	response := newResponse(resp)

	err = CheckResponse(resp)
	if err != nil {
		return response, err
	}

	var json_resp = &ListResponse{Data: v}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			_, err = io.Copy(w, resp.Body)
		} else {
			err = json.NewDecoder(resp.Body).Decode(json_resp)
		}
	}
	return response, err
}
