package norad

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestOrganizationConfigurationsServiceParseError(t *testing.T) {
	organization_configuration := &OrganizationConfigurationsService{}
	_, _, err := organization_configuration.Get(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	organization_configuration = &OrganizationConfigurationsService{}
	_, _, err = organization_configuration.Update(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}
}

func TestOrganizationConfigurationsServiceGet(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a OrganizationConfiguration with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"organizations/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			organization_obj := new(Organization)
			client.On(
				"Do",
				&http.Request{},
				organization_obj,
			).Return(&Response{}, c.doError)

			organization_configuration := &OrganizationConfigurationsService{client: &client}
			_, _, err := organization_configuration.Get(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestOrganizationConfigurationsServiceUpdate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a OrganizationConfiguration with expected error %s", c.expectedError), func(t *testing.T) {
			opts := OrganizationConfigurationOptions{Parameters: OrganizationConfigurationParameters{UseRelaySshKey: true}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"PUT",
				"organization_configurations/1",
				&opts,
			).Return(&http.Request{}, c.requestError)
			organization_configuration_obj := new(OrganizationConfiguration)
			client.On(
				"Do",
				&http.Request{},
				organization_configuration_obj,
			).Return(&Response{}, c.doError)

			organization_configuration := &OrganizationConfigurationsService{client: &client}
			_, _, err := organization_configuration.Update(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}
