package norad

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

func TestNotificationChannelServiceParseError(t *testing.T) {
	service := &NotificationChannelsService{}
	_, _, err := service.List(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &NotificationChannelsService{}
	_, _, err = service.Get(false)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}

	service = &NotificationChannelsService{}
	_, _, err = service.Update(false, nil)
	if err == nil || !strings.Contains(err.Error(), "invalid ID type false") {
		t.Errorf("Expected parse error, but got %s", err)
	}
}

func TestNotificationChannelsServiceList(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing listing NotificationChannels with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"organizations/1/notification_channels",
				nil,
			).Return(&http.Request{}, c.requestError)
			notification_channels := make([]*NotificationChannel, 0)
			client.On(
				"Do",
				&http.Request{},
				&notification_channels,
			).Return(&Response{}, c.doError)

			service := &NotificationChannelsService{client: &client}
			_, _, err := service.List(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestNotificationChannelsServiceUpdate(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a NotificationChannel with expected error %s", c.expectedError), func(t *testing.T) {
			opts := NotificationChannelOptions{Parameters: NotificationChannelParameters{Enabled: true}}
			client := MockClient{}
			client.On(
				"NewRequest",
				"PUT",
				"notification_channels/1",
				&opts,
			).Return(&http.Request{}, c.requestError)
			notification_channel := new(NotificationChannel)
			client.On(
				"Do",
				&http.Request{},
				notification_channel,
			).Return(&Response{}, c.doError)

			service := &NotificationChannelsService{client: &client}
			_, _, err := service.Update(1, &opts)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}

func TestNotificationChannelsServiceGet(t *testing.T) {
	cases := []struct {
		requestError  error
		doError       error
		expectedError error
	}{
		{requestError: errors.New("request_error"), expectedError: errors.New("request_error")},
		{doError: errors.New("do_error"), expectedError: errors.New("do_error")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing getting a NotificationChannel with expected error %s", c.expectedError), func(t *testing.T) {
			client := MockClient{}
			client.On(
				"NewRequest",
				"GET",
				"notification_channels/1",
				nil,
			).Return(&http.Request{}, c.requestError)
			notification_channel_obj := new(NotificationChannel)
			client.On(
				"Do",
				&http.Request{},
				notification_channel_obj,
			).Return(&Response{}, c.doError)

			service := &NotificationChannelsService{client: &client}
			_, _, err := service.Get(1)
			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, but got %s", err)
			}
		})
	}
}
