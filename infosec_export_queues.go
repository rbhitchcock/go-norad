package norad

import (
	"fmt"
)

type InfosecExportQueuesService struct {
	client NoradClient
}

type InfosecExportQueue struct {
	ResultExportQueue
	QueueConfig InfosecExportQueueConfiguration `json:"infosec_export_queue_configuration"`
}

type InfosecExportQueueUpdateOptions struct {
	Parameters InfosecExportQueueUpdateParameters `json:"infosec_export_queue"`
}

type InfosecExportQueueCreateOptions struct {
	Parameters InfosecExportQueueCreateParameters `json:"infosec_export_queue"`
}

type InfosecExportQueueUpdateParameters struct {
	AutoSync    bool                            `json:"auto_sync"`
	QueueConfig InfosecExportQueueConfiguration `json:"infosec_export_queue_configuration_attributes"`
}

type InfosecExportQueueCreateParameters struct {
	AutoSync    bool                                  `json:"auto_sync"`
	QueueConfig InfosecExportQueueConfigCreateOptions `json:"infosec_export_queue_configuration_attributes"`
}

type InfosecExportQueueConfigCreateOptions struct {
	CtsmId string `json:"ctsm_id"`
}

type InfosecExportQueueConfiguration struct {
	Id     int    `json:"id"`
	CtsmId string `json:"ctsm_id"`
}

func (obj InfosecExportQueue) String() string {
	return Stringify(obj)
}

func (service *InfosecExportQueuesService) Create(oid interface{}, opt *InfosecExportQueueCreateOptions) (*InfosecExportQueue, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/infosec_export_queues", org_id)
	req, err := service.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	infosec_export_queue := new(InfosecExportQueue)
	resp, err := service.client.Do(req, infosec_export_queue)
	if err != nil {
		return nil, resp, err
	}

	return infosec_export_queue, resp, err
}

func (service *InfosecExportQueuesService) Delete(id interface{}) (*Response, error) {
	infosec_export_queue_id, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("infosec_export_queues/%s", infosec_export_queue_id)
	req, err := service.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return service.client.Do(req, nil)
}

func (service *InfosecExportQueuesService) Get(id interface{}) (*InfosecExportQueue, *Response, error) {
	channel_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	// We have to look up the associated org and pluck its config
	u := fmt.Sprintf("infosec_export_queues/%s", channel_id)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	infosec_export_queues := new(InfosecExportQueue)
	resp, err := service.client.Do(req, infosec_export_queues)
	if err != nil {
		return nil, resp, err
	}

	return infosec_export_queues, resp, err
}

func (service *InfosecExportQueuesService) Update(id interface{}, opt *InfosecExportQueueUpdateOptions) (*InfosecExportQueue, *Response, error) {
	channel_id, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("infosec_export_queues/%s", channel_id)
	req, err := service.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	infosec_export_queues := new(InfosecExportQueue)
	resp, err := service.client.Do(req, infosec_export_queues)
	if err != nil {
		return nil, resp, err
	}

	return infosec_export_queues, resp, err
}
