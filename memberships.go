package norad

import (
	"fmt"
)

type MembershipsService struct {
	client NoradClient
}

type Membership struct {
	Id             int
	OrganizationId int `json:"organization_id"`
	UserId         int `json:"user_id"`
	Role           string
	User           *User
}

type MembershipOptions struct {
	Parameters MembershipParameters `json:"membership"`
}

type MembershipParameters struct {
	Uid   bool
	Admin bool
}

func (m Membership) String() string {
	return Stringify(m)
}

func (service *MembershipsService) List(oid interface{}) ([]*Membership, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/memberships", org_id)
	req, err := service.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	memberships := make([]*Membership, 0)
	resp, err := service.client.Do(req, &memberships)
	if err != nil {
		return nil, resp, err
	}

	return memberships, resp, err
}

func (service *MembershipsService) Create(oid interface{}, opt *MembershipOptions) (*Membership, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/memberships", org_id)
	req, err := service.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	membership := new(Membership)
	resp, err := service.client.Do(req, membership)
	if err != nil {
		return nil, resp, err
	}

	return membership, resp, err
}

func (service *MembershipsService) Delete(id interface{}) (*Response, error) {
	membership_id, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("memberships/%s", membership_id)
	req, err := service.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return service.client.Do(req, nil)
}
