package norad

import (
	"fmt"
)

type MachinesService struct {
	client *Client
}

type Machine struct {
	Id             int    `json:"id"`
	Ip             string `json:"ip"`
	Fqdn           string `json:"fqdn"`
	Description    string `json:"description"`
	Name           string `json:"name"`
	OrganizationId int    `json:"organization_id"`
}

type CreateMachineOptions struct {
	MachineParameters `json:"machine"`
	OrganizationToken string `json:"organization_token"`
}

type UpdateMachineOptions struct {
	MachineParameters `json:"machine"`
}

type MachineParameters struct {
	Ip             string `json:"ip"`
	Fqdn           string `json:"fqdn"`
	Description    string `json:"description"`
	Name           string `json:"name"`
	OrganizationId int    `json:"organization_id"`
}

func (m Machine) String() string {
	return Stringify(m)
}

func (s *MachinesService) ListMachines(oid interface{}) ([]*Machine, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/machines", org_id)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o []*Machine
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *MachinesService) GetMachine(id interface{}) (*Machine, *Response, error) {
	mid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("machines/%s", mid)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *Machine
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *MachinesService) CreateMachine(oid interface{}, opt *CreateMachineOptions) (*Machine, *Response, error) {
	org_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("organizations/%s/machines", org_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(Machine)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *MachinesService) UpdateMachine(id interface{}, opt *UpdateMachineOptions) (*Machine, *Response, error) {
	mid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("machines/%s", mid)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(Machine)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *MachinesService) DeleteMachine(id interface{}) (*Response, error) {
	mid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("machines/%s", mid)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
