package norad

import (
	"fmt"
)

type WebApplicationServicesService struct {
	client *Client
}

type WebApplicationService struct {
	Id              int    `json:"id"`
	Name            string `json:"name"`
	Description     string `json:"description"`
	Port            int    `json:"port"`
	PortType        string `json:"port_type"`
	EncryptionType  string `json:"encryption_type"`
	Type            string `json:"type"`
	AllowBruteForce bool   `json:"allow_brute_force"`
	Discovered      bool   `json:"discovered"`
	MachineId       int    `json:"machine_id"`
}

type CreateWebApplicationServiceOptions struct {
	WebApplicationServiceCreationParameters `json:"service"`
}

type UpdateWebApplicationServiceOptions struct {
	WebApplicationServiceUpdateParameters `json:"service"`
}

type WebApplicationServiceCreationParameters struct {
	Name            string `json:"name"`
	Description     string `json:"description"`
	Port            string `json:"port"`
	PortType        string `json:"port_type"`
	EncryptionType  string `json:"encryption_type"`
	Type            string `json:"type"`
	AllowBruteForce bool   `json:"allow_brute_force"`
}

type WebApplicationServiceUpdateParameters struct {
	Name            string `json:"name"`
	Description     string `json:"description"`
	Port            string `json:"port"`
	PortType        string `json:"port_type"`
	EncryptionType  string `json:"encryption_type"`
	AllowBruteForce bool   `json:"allow_brute_force"`
}

func (m WebApplicationService) String() string {
	return Stringify(m)
}

func (s *WebApplicationServicesService) GetWebApplicationService(id interface{}) (*WebApplicationService, *Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}

	u := fmt.Sprintf("services/%s", sid)
	req, err := s.client.NewRequest("GET", u, nil)

	if err != nil {
		return nil, nil, err
	}

	var o *WebApplicationService
	resp, err := s.client.Do(req, &o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *WebApplicationServicesService) CreateWebApplicationService(oid interface{}, opt *CreateWebApplicationServiceOptions) (*WebApplicationService, *Response, error) {
	machine_id, err := parseID(oid)
	if err != nil {
		return nil, nil, err
	}
	opt.WebApplicationServiceCreationParameters.Type = "WebApplicationService"
	opt.WebApplicationServiceCreationParameters.PortType = "tcp"

	u := fmt.Sprintf("machines/%s/services", machine_id)
	req, err := s.client.NewRequest("POST", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(WebApplicationService)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *WebApplicationServicesService) UpdateWebApplicationService(id interface{}, opt *UpdateWebApplicationServiceOptions) (*WebApplicationService, *Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, nil, err
	}
	opt.WebApplicationServiceUpdateParameters.PortType = "tcp"

	u := fmt.Sprintf("services/%s", sid)
	req, err := s.client.NewRequest("PUT", u, opt)
	if err != nil {
		return nil, nil, err
	}

	o := new(WebApplicationService)
	resp, err := s.client.Do(req, o)
	if err != nil {
		return nil, resp, err
	}

	return o, resp, err
}

func (s *WebApplicationServicesService) DeleteWebApplicationService(id interface{}) (*Response, error) {
	sid, err := parseID(id)
	if err != nil {
		return nil, err
	}

	u := fmt.Sprintf("services/%s", sid)
	req, err := s.client.NewRequest("DELETE", u, nil)
	if err != nil {
		return nil, err
	}
	return s.client.Do(req, nil)
}
